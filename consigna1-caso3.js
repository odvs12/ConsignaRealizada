// ### CASO 3

// Tengo las siguientes funciones:

function buscarEnFacebook(texto, callback) {
  /* Hace algunas cosas y las guarda en "result" */
  if (result.error) {
    callback(error, result.error)
  } else {
    callback(null, result.data);
  }
}
function buscarEnGithub(texto, callback) {
  /* Hace algunas cosas y las guarda en "result" */
  if (result.error) {
    callback(error, result.error)
  } else {
    callback(null, result.data);
  }
}



//   - ¿Qué debería hacer para usar la funcionalidad con promesas y no callbacks?

function buscarEnFacebook(texto, callback) {
  /* Hace algunas cosas y las guarda en "result" */
  if (result.error) {
    callback(error, result.error)
  } else {
    callback(null, result.data);
  }
}
function buscarEnGithub(texto, callback) {
  /* Hace algunas cosas y las guarda en "result" */
  if (result.error) {
    callback(error, result.error)
  } else {
    callback(null, result.data);
  }
}

//   - ¿Podés replicar la siguiente API?
    // ```javascript
    buscador('hola')
    .facebook()
    .github()
    .then((data) => {
      // data[0] = data de Facebook
      // data[1] = data de GitHub
    })
    .catch(error => {
      // error[0] = error de Facebook
      // error[1] de GitHub
    })
//     ```
//   - y a la solución anterior
//     - ¿Cómo podrías agregarle otra búsqueda?
//     - ¿Cómo solucionas el problema de si una API entrega un error, mientras las otras devuelven data?