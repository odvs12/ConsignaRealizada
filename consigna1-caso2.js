var endorsements = [
    { skill: 'css', user: 'Bill' },
    { skill: 'javascript', user: 'Chad' },
    { skill: 'javascript', user: 'Bill' },
    { skill: 'css', user: 'Sue' },
    { skill: 'javascript', user: 'Sue' },
    { skill: 'html', user: 'Sue' }
];
/*
¿Cómo podrías ordenarlo de la siguiente forma?:

[
    { skill: 'css', users: [ 'Bill', 'Sue', 'Sue' ], count: 2 },
    { skill: 'javascript', users: [ 'Chad', 'Bill', 'Sue' ], count: 3 },
    { skill: 'html', users: [ 'Sue' ], count: 1 }
]
*/

function filtrarArray(array) {
    let arrayCss = [];
    let arrayJavascript = [];
    let arrayHtml = [];
    
    array.forEach(element => {
        if (element.skill === 'css') {
            arrayCss.push(element.user);
        } else if (element.skill === 'javascript'){
            arrayJavascript.push(element.user)    
        } else if (element.skill === 'html') {
            arrayHtml.push(element.user)    
        }   
    });

    arrayCss.push(`count: ${arrayCss.length}`)
    arrayJavascript.push(`count: ${arrayJavascript.length}`)
    arrayHtml.push(`count: ${arrayHtml.length}`)

    console.log(arrayCss)
    console.log(arrayJavascript)
    console.log(arrayHtml)
}

filtrarArray(endorsements)