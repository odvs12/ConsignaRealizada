// ### ALGORITMOS

/*Escribir una función simple (no más de 100 caracteres) que retorne un boolean indicando si un string
 dado es un palíndromo.*/

function palindromo(palabra) {
    return palabra === palabra.split('').reverse().join('');
}

palindromo('arepera');

// ------------------------------------------------------------------

// ### JAVASCRIPT 

// What will the code below output to the console and why?

var myObject = {
    foo : "bar",
    func:function() {
        var self=this;
        console.log("outer func: this.foo = "+this.foo);
        console.log("outer func: self.foo = "+self.foo);
        (function(){
            console.log("inner func: this.foo = " +this.foo);
            console.log("inner func: self.foo = " +self.foo);   
        }());
    }
};

myObject.func();

/*
    este es el output del console.log()
    
        outer func: this.foo = bar
        outer func: self.foo = bar
        inner func: this.foo = undefined
        inner func: self.foo = bar

    Estos métodos retonan el valor de la variable "foo" excepto el que usa "this.foo" en el método 
    anidado al método "func", esto es por el "scope" del "this". Mientras la variable "self" tiene 
    un "scope" completo dentro del método "func", "this" solo puede referir hasta un nivel superior 
    a menos que se haga "bind()" a una propiedad que quiere alcanzar el "this" 

---------------------------------------------------------------

a. What is a Promise?
    Una promesa es un objeto que representa el eventual exito o error de una función asincrona, 
    esto fué incluido en ECMAScript 6 y es la mejor y más clara manera de trabajar de forma Asíncrona 
    con callbacks, antes los callback podian llegar a causar el "callback Hell".

---------------------------------------------------------------

b. What is ECMAScript?
    ECMAScript es un estandar hecho por ECMA, este nace para estandarizar JavaScript, al ser un estandar, 
    otros lenguajes pueden basarse en él, sin embargo, el principal es JavaScript. 

    Por esto las versiones se llaman "ECMAScript x" y no "JavaScript x" 

---------------------------------------------------------------

c. What is NaN? What is its type? How can you reliably test if a value is equal to NaN ?
    NaN (Not a Number) es un tipo de dato Falsy pero además, es una propiedad del Global Object, 
    algo curioso de NaN es que en una comparación de igualdad no NaN no es igual a NaN.

    La forma de testear si un valor es igual a NaN es usando el método isNaN(), de esta forma 
    SI se puede compara NaN con NaN y va a dar el "true", lo que hace este método es convertir 
    lo que se le pase entre parentesis a un número, si no se puede entonces su resultado es false, 
    por ejemplo:
    
        * isNaN(5.6) = false
        * isNaN(5,6) = false
        * isNaN('Hola') = true
*/

//------------------------------------------------------------------

// d. What will be the output when the following code is executed? Explain.

console.log(false=='0')
console.log(false==='0')

/*
*false =='0' 
    Es true porque 0 es un valor falsy, entonces, en la "igualdad debil" por el type cohersion, 
    con una propiedad que sea igual, la igualdad/comparación retorna true,  

*false==='0'
    Es false porque en la igualdad estrica no hay "type cohersion" y el único valor igual a '0' es '0'

------------------------------------------------------------------

### Node

Explain what does "event-driven, non-blocking I/O" means in Javascript.
    
    Event-driven 
        Es un paradigma de programación donde todo el flujo de la aplicación es determinada 
        a los eventos que surjan en la interacción del Cliente con la app.

    Non-blocking I/O 
        Significa que cada nuevo evento no bloquea la realización de teareas ejecutadas por 
        eventos anteriores. NodeJs funciona con un "event loop" de "un solo hilo" que envia 
        los eventos (req) a otra instancia para que estos se vayan realizando sin bloquear 
        los nuevos eventos que lleguen, cuando la tarea ya se ha realizado, devuelve la respuesta 
        (res) por el mismo "event loop" que la trajo para que sea enviada al Cliente.

        Esto es lo que hace que nodeJs sea muy veloz en tareas como streaming de audio y video 
        o funcione bien en chats donde se envian y reciben datos sin realizar ninguna operación 
        extra en estos.
*/