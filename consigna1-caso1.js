var Foo = function( a ) {
/*en la Clase, la variable no estaria trabajando para referir al valor 
contextual dado en "f" que hereda de "Foo" (this si lo hace)*/

    // var baz = function() {
    //     return a;
    // };

    this.baz = function() {
        return a;
    };

    this.bar = function() {
        return a;
    };

    this.biz = function() {
        return a;
    }
};

/*Prototype sirve para heredar del prototipo de una función, 
en este caso no tendria sentido que Foo se heredara a si mismo y
el código no está heredando a nadie en todo caso (está incompleto).*/

// Foo.prototype = {
//     biz: function() {
//         return a;
//     }
// };

var f = new Foo( 7 );
console.log(f.bar());
console.log(f.baz());
console.log(f.biz());

/*
1. ¿Cuál es el resultado de las últimas 3 líneas?
    * f.bar();
        TypeError: f.bar is not a function

    * f.baz();
        7

    * f.biz();
        ReferenceError: a is not defined

2. Modificá el código de `f.bar()` para que retorne `7`?

3. Modificá el código para que `f.biz()` también retorne `7`?

*/